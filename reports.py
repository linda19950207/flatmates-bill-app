import webbrowser
from fpdf import FPDF
import os


class PdfReport:
    """
    create a PDF file that contains data about the flatemates
    such as their names, their dueamount and the period of the bill.
    """

    def __init__(self, filename):
        self.filename = filename

    def generate(self, flatmate1, flatmate2, bill):
        pdf = FPDF(orientation="P", unit="pt", format="A4")
        pdf.add_page()

        # Add title image
        pdf.image("house.png", w=30, h=30)

        # Insert Title
        pdf.set_font(family="Times", size=24, style="B")
        pdf.cell(w=0, h=80, txt="Flatmates Bill", border=1, align="C", ln=1)

        # Insert Period label and value
        pdf.set_font(family="Times", size=14, style="B")
        pdf.cell(w=100, h=40, txt="Period", border=1)
        pdf.cell(w=150, h=40, txt=bill.period, border=1, ln=1)

        # Insert name and due amount of the first flatmate
        pdf.set_font(family="Times", size=12)
        pdf.cell(w=100, h=40, txt=flatmate1.name, border=1)
        pdf.cell(w=150, h=40, txt=str(round(flatmate1.payment(bill, flatmate2), 2)), border=1, ln=1)

        # Insert name and due amount of the second flatmate
        pdf.set_font(family="Times", size=12)
        pdf.cell(w=100, h=40, txt=flatmate2.name, border=1)
        pdf.cell(w=150, h=40, txt=str(round(flatmate2.payment(bill, flatmate1), 2)), border=1, ln=1)

        # Save all the bills to the Bill_files for organization purpose
        os.chdir("Bill_files")
        pdf.output(self.filename)
        webbrowser.open(self.filename)
